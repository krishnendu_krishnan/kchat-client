export company="appden"
export software="kchat"
export version="latest"
#docker-compose -f ./setup/docker/docker-compose.yml up -d
docker build -t ${company}/${software}:${version} .
docker tag ${company}/${software}:${version} 993537197246.dkr.ecr.us-east-1.amazonaws.com/${company}/${software}:${version}

login="$(aws ecr get-login --no-include-email --region us-east-1)"
${login}

docker push 993537197246.dkr.ecr.us-east-1.amazonaws.com/${company}/${software}:${version}

